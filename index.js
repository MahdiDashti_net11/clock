let time = new Date()

let year = time.getFullYear()
// let month = time.getMonth() + 1
let day = time.getDate()
let monthName = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
let thisMonth = monthName[time.getMonth()];
console.log(thisMonth)
const calendar = document.querySelector('#showdate')

calendar.innerHTML = day + " " + thisMonth + " " + year

const secondHand = document.querySelector('.second-hand')
const minHand = document.querySelector('.min-hand')
const hourHand = document.querySelector('.hour-hand')

function setDate() {
    const now = new Date();
    const seconds = now.getSeconds();
    const secondDegrees = ((seconds / 60) * 360) + 90;
    secondHand.style.transform = `rotate(${secondDegrees}deg)`

    const mins = now.getMinutes();
    const minsDegrees = ((mins / 60) * 360) + 90;
    minHand.style.transform = `rotate(${minsDegrees}deg)`

    const hours = now.getHours();
    const hoursDegrees = ((hours / 12) * 360) + 90;
    hourHand.style.transform = `rotate(${hoursDegrees}deg)`
    { console.log(hours, mins, seconds) }
}

setInterval(setDate, 1000)




let showData = document.querySelector('#show')
let daylist = ["یک شنبه", "دوشنبه", "سه شنبه", "چهارشنبه ", "پنج شنبه", "جمعه", "شنبه"];

if (time.getDay() == 0) {
    showData.innerHTML = `Today is : Sunday`
}
if (time.getDay() == 1) {
    showData.innerHTML = `Today is : Monday`
}
if (time.getDay() == 2) {
    showData.innerHTML = `Today is : Tuesday`
}
if (time.getDay() == 4) {
    showData.innerHTML = `Today is : Thursday`
}
if (time.getDay() == 5) {
    showData.innerHTML = `Today is : Friday`
}
if (time.getDay() == 6) {
    showData.innerHTML = `Today is : Saturday`
}
let showTime = document.querySelector('#showTime')
function show() {
    let time1 = new Date()
    let hour = time1.getHours()
    let second = time1.getSeconds()
    let min = time1.getMinutes()

    showTime.innerHTML = ` ${hour > 9 ? hour : "0" + hour} : ${min > 9 ? min : "0" + min} : ${second > 9 ? second : "0" + second}`
}
setInterval(show, 1000)



